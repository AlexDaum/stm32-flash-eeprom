# Overview
This library implements a very simple eeprom emulation in the embedded flash of an STM32 controller in c++ with direct register acces, so no Cube HAL or the std_periph library is not needed. It uses 2 pages to enable wear leveling by first writing a whole page full of data, then compressing it to the second page and erasing the first.  
Only 16bit Data values are supported, because implementation was simpler this way and the library offers no protection against power loss during page compression, the data could get corrupted when that happens.  
In addition to the data a 16bit id is stored in combination with the data to identify the value.

This was written for the STM32F030, but should be portable pretty easily.

# Usage

## Defining the section
In order to use this library, you have to define a section containing two pages in the linker script.  
For simplicity the last two pages can be used.

The start and end of the eeprom section as well as the size of a page also have to be set in flash_defs.h.

## Using in a program

To use the library, add the sources and headers to your project either by copying them or linking to them in your IDE.  
Then include flash_eeprom.h in every file, that needs to acces the eeprom emulation.

This header declares the symbol `eeprom`, which manages all access to the emulated eeprom. The interface can be used with the [] operator like an array.  
e.g. to write the value at id 0 to 10 the code `eeprom[0] = 10` is used.  
likewise `uint16_t value = eeprom[0]` reads the value corresponding to id 0.  

If there is no value associated to the id, the read method returns 0xFFFF (or -1 if signed).

To check if the value is truly empty or set to 0xFFFF, the method `bool eeprom.isEmpty(uint16_t id)` can be used. It returns true if the value is empty, false otherwise.