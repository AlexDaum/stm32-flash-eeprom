//
// Copyright (C) 2019 Alexander Daum
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include "flash_eeprom.h"

flash_eeprom::flash_eeprom() {
	currentEntry = (eeprom_entry *)EEPROM_START;
	if (currentEntry->id == 0xFFFF) {
		// check if second page contains data
		eeprom_entry *page2 =
			(eeprom_entry *)(EEPROM_START + flash_utils::PAGE_SIZE);
		if (page2->id != 0xFFFF)
			currentEntry = page2;
	}

	for (; (uintptr_t)currentEntry < FLASH_END && currentEntry->id != 0xFFFF;
		 currentEntry++) { // find first empty
	}
	currentEntry--; // set to last not empty
}

eeprom_data flash_eeprom::operator[](uint16_t id) { return eeprom_data(id); }

static eeprom_entry *getPageBegin(eeprom_entry *ptr) {
	return (eeprom_entry *)((uintptr_t)ptr & ~(PAGE_SIZE_BYTE - 1));
}

uint16_t flash_eeprom::get(uint16_t id) const {
	eeprom_entry *start = getPageBegin(currentEntry);
	uint16_t value = 0xFFFF;
	for (; start <= currentEntry; start++) {
		if (start->id == id)
			value = start->value;
	}
	return value;
}

void flash_eeprom::gc() {
	// currentEntry points to the first address in a new page
	// so we have to subtract one before getting the beginning of the old page
	// Also we want to start at the boundary the next time writing and it
	// increments before writing, so decrement it here
	currentEntry--;
	eeprom_entry *oldPage = getPageBegin(currentEntry);

	if ((uintptr_t)currentEntry >= FLASH_END)
		currentEntry = (eeprom_entry *)EEPROM_START;

	// Now copy the newest value for each id to the new page
	const size_t entries_in_page = PAGE_SIZE_BYTE / sizeof(eeprom_entry);
	std::array<uint32_t, entries_in_page / 32>
		processed; // set of bits that indicate which fields were already
				   // processed
	processed.fill(0);
	for (size_t i = 0; i < entries_in_page; i++) {
		uint32_t &procFieldOuter = processed[i / 32];
		uint32_t procMask = 1 << (i % 32);
		if (procFieldOuter & procMask)
			continue;
		eeprom_entry *ent = oldPage + i;
		eeprom_entry latest = *ent;
		procFieldOuter |= procMask;
		// find latest with the same id
		for (size_t j = i + 1; j < entries_in_page; j++) {
			uint32_t &procFieldInner = processed[j / 32];
			procMask = 1 << (j % 32);
			if (procFieldInner & procMask)
				continue;
			ent = oldPage + j;
			if (ent->id == latest.id) {
				latest = *ent;
				procFieldInner |= procMask;
			}
		}
		currentEntry++;
		doWrite(latest.id, latest.value); // write to new page
	}
	flash_utils::erase(oldPage);
}


bool flash_eeprom::isEmpty(uint16_t id){
	eeprom_entry *start = getPageBegin(currentEntry);
	for (; start <= currentEntry; start++) {
		if (start->id == id)
			return false;
	}
	return true;
}

void flash_eeprom::doWrite(uint16_t id, uint16_t value) {
	uint16_t *ptr = (uint16_t *)currentEntry;
	flash_utils::write(ptr, id);
	flash_utils::write(ptr + 1, value);
}

void flash_eeprom::write(uint16_t id, uint16_t value) {
	currentEntry++;
	if ((uintptr_t)currentEntry > EEPROM_START &&
		((uintptr_t)currentEntry % PAGE_SIZE_BYTE) == 0) {
		// hit a page boundary
		gc();
	}
	doWrite(id, value);
}
