//
// Copyright (C) 2019 Alexander Daum
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include "stm.h"
#include <flash_utils.h>

void flash_utils::unlock_flash() {
	if (FLASH->CR & FLASH_CR_LOCK) {
		FLASH->KEYR = 0x45670123;
		FLASH->KEYR = 0xCDEF89AB;
	}
}

void flash_utils::write(uint16_t *addr, uint16_t val) {
	unlock_flash();
	while (FLASH->SR & FLASH_SR_BSY)
		;
	FLASH->CR |= FLASH_CR_PG;
	*addr = val;
	while (FLASH->SR & FLASH_SR_BSY)
		;
	FLASH->SR = FLASH_SR_EOP;
	FLASH->CR &= ~FLASH_CR_PG;
}

void flash_utils::erase(void *page) {
	unlock_flash();
	while (FLASH->SR & FLASH_SR_BSY)
		;
	FLASH->CR |= FLASH_CR_PER;
	uintptr_t upage = (uintptr_t) page;
	FLASH->AR = upage;
	FLASH->CR |= FLASH_CR_STRT;
	while (FLASH->SR & FLASH_SR_BSY)
		;
	FLASH->SR = FLASH_SR_EOP;
	FLASH->CR &= ~FLASH_CR_PER;
}
